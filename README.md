# Quick Links
- :computer: Need the driver or legacy `vlviewer` application?
    - Download the [EDT PDV Driver](https://edt.com/file-category/pdv/).
    - Download the legacy `vlviewer`
      - Qt4 Versions: [linux](https://gitlab.com/engineering-design-team/vlviewer/-/blob/master/legacy/vlviewer_v5.6.3.0_qt4.tar.gz), [windows 64 bit](https://gitlab.com/engineering-design-team/vlviewer/-/blob/master/legacy/vlviewer_v5.6.3.0_qt4-amd64.zip), [windows 32 bit](https://gitlab.com/engineering-design-team/vlviewer/-/blob/master/legacy/vlviewer_v5.6.3.0_qt4-x86.zip)
      - Qt5 Versions: [linux](https://gitlab.com/engineering-design-team/vlviewer/-/blob/master/legacy/vlviewer_v5.6.3.0_qt5.tar.gz)
- :question: Have a bug, question, or feature request? Please, submit an [issue](https://gitlab.com/engineering-design-team/vlviewer/-/issues).
- :book: Read the [vlviewer docs](https://gitlab.com/engineering-design-team/vlviewer/-/wikis/home).

# About `vlviewer`
The `vlviewer` application is a supplemental application intended for use with the [EDT PDV driver](https://edt.com/file-category/pdv/).

The `vlviewer` application serves two main purposes:
- Provide a simple interface for validating camera configurations and saving images being captured
from [EDT framegrabbers](https://edt.com/product-lines/camera-link-frame-grabbers/).
- Give a baseline application to reference while building a custom application using
[EDT framegrabbers](https://edt.com/product-lines/camera-link-frame-grabbers/).
